from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from topsum.models import GlobalMarketEnv, Market, MarketPeriod, Employee,\
                          Company, CompanyPeriod, Product, ProductPeriod, Period

class EmployeeInline(admin.StackedInline):
    max_num = 1
    model = Employee
    can_delete = False
    verbose_name_plural = "employee"

class UserAdmin(BaseUserAdmin):
    inlines = (EmployeeInline, )

# User our own User Admin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

#admin.site.register(Employee)
class GlobalMarketEnvAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
admin.site.register(GlobalMarketEnv, GlobalMarketEnvAdmin)
class MarketAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
admin.site.register(Market, MarketAdmin)
admin.site.register(MarketPeriod)
class CompanyAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyPeriod)
class ProductAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductPeriod)
admin.site.register(Period)
