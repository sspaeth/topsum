from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin, AccessMixin
from django.contrib.auth.models import Group
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from topsum.models import GlobalMarketEnv, Market, MarketPeriod, Employee,\
                          Company, CompanyPeriod, Product, ProductPeriod

class UserIsTutorMixin(UserPassesTestMixin, AccessMixin):
    raise_exception=False # Allow anonymous users to login
    permission_denied_message ="Only Tutors or staff have the power to view this page. It is not for you, sorry!"
    def test_func(self):
        if self.request.user.is_staff: return True
        try:
          self.request.user.groups.get(name="Tutor")
        except Group.DoesNotExist:
            return False
        return True
            

def user_is_tutor():
    """test_func deciding if user is a tutor"""
    return False

class GlobalMarketEnvListView(UserIsTutorMixin, ListView):
    model = GlobalMarketEnv

class GlobalMarketEnvView(LoginRequiredMixin, DetailView):
    model = GlobalMarketEnv
    ### TODO: Allow only employees in this game and tutors to view this

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object
        return context

class GlobalMarketEnvCreateView(UserIsTutorMixin, CreateView):
    model = GlobalMarketEnv
    fields = ['name', 'slug','total_periods', 'status']

class GlobalMarketEnvUpdateView(UserIsTutorMixin, UpdateView):
    model = GlobalMarketEnv
    fields = ['name', 'slug','total_periods', 'status']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object
        return context

def GlobalMarketEnv_advance_view(pk):
    return "MOO"

class CompanyListView(LoginRequiredMixin, ListView):
    model = Company
    #TODO: filter by game
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object.globalenv
        return context

class CompanyView(LoginRequiredMixin, DetailView):
    model = Company

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object.globalenv
        return context

class CompanyCreateView(UserIsTutorMixin, CreateView):
    model = Company
    fields = ['name', 'slug','logo']
    
    #def get_initial(self):
    #    initial = super().get_initial()
    #    get_object_or_404(GlobalMarketEnv, id=self.kwargs.get('game_id'))
    #    initial['globalenv']= self.kwargs.get('game_id')
    #    return initial
        
    def form_valid(self, form):
        company = form.save(commit=False)
        globalenv = GlobalMarketEnv(id=self.kwargs.get('game_id'))
        company.globalenv = globalenv
        company.save()
        form.save_m2m()
        self.object = company
        return redirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = GlobalMarketEnv.objects.get(id=self.kwargs.get('game_id'))
        return context

    def get_success_url(self):
        messages.success(self.request, 'Company successfully created.')
        return self.request.GET.get('next') or super().get_success_url()


class CompanyUpdateView(UserIsTutorMixin, UpdateView):
    model = Company
    fields = ['name', 'slug','logo']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object.globalenv
        return context

    def get_success_url(self):
        messages.success(self.request, 'Company details updated.')
        return self.request.GET.get('next') or super().get_success_url()

        
class CompanyPeriodView(LoginRequiredMixin, DetailView):
    model = CompanyPeriod
    
    def get_object(self, queryset=None):
        """Show CompanyPeriod"""
        period = self.kwargs.get('p')            
        comp = get_object_or_404(Company, pk=self.kwargs.get('comp'))
        cur_period = comp.globalenv.current_period
        if period > cur_period:
            raise Http404("You are in the future!")
        if period < 1:
            raise Http404("Time must be strictly positive!")
        return get_object_or_404(CompanyPeriod, company=comp, period=period)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['company'] = self.object.company
        context['globalenv'] = self.object.company.globalenv
        return context

class CompanyPeriodNowView(CompanyPeriodView):
    """Show CompanyPeriod in this current period"""
    def get_object(self, queryset=None):
        """Create a matching CompanyPeriod on the fly if needed

        It will use the model default values for actions"""
        comp = get_object_or_404(Company, pk=self.kwargs.get('comp'))
        self.kwargs['p'] = comp.globalenv.current_period
        return super().get_object(queryset)


class MarketView(LoginRequiredMixin, DetailView):
    model = Market

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object.globalenv
        return context

class MarketUpdateView(UserIsTutorMixin, UpdateView):
    model = Market
    fields = ['name', 'slug' ]

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object.globalenv
        return context

    def get_success_url(self):
        messages.success(self.request, 'Market details updated.')
        return self.request.GET.get('next') or super().get_success_url()

class MarketCreateView(UserIsTutorMixin, CreateView):
    model = Market
    fields = ['name', 'slug']

    def form_valid(self, form):
        market = form.save(commit=False)
        globalenv = GlobalMarketEnv(id=self.kwargs.get('game_id'))
        market.globalenv = globalenv
        market.save()
        form.save_m2m()
        self.object = market
        return redirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = GlobalMarketEnv.objects.get(id=self.kwargs.get('game_id'))
        return context

    def get_success_url(self):
        messages.success(self.request, 'Market successfully created.')
        return self.request.GET.get('next') or super().get_success_url()

class ProductView(LoginRequiredMixin, DetailView):
    model = Product

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object.globalenv
        return context

class ProductCreateView(UserIsTutorMixin, CreateView):
    model = Product
    fields = ['name', 'slug', 'intro_period']

    def form_valid(self, form):
        product = form.save(commit=False)
        globalenv = GlobalMarketEnv(id=self.kwargs.get('game_id'))
        product.globalenv = globalenv
        product.save()
        form.save_m2m()
        self.object = product
        return redirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = GlobalMarketEnv.objects.get(id=self.kwargs.get('game_id'))
        return context

    def get_success_url(self):
        messages.success(self.request, 'Product details updated.')
        return self.request.GET.get('next') or super().get_success_url()


class ProductUpdateView(UserIsTutorMixin, UpdateView):
    model = Product
    fields = ['name', 'slug', 'intro_period']

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['globalenv'] = self.object.globalenv
        return context

    def get_success_url(self):
        messages.success(self.request, 'Product details updated.')
        return self.request.GET.get('next') or super().get_success_url()


class ProductPeriodView(LoginRequiredMixin, DetailView):
    model = ProductPeriod
    
    def get_object(self, queryset=None):
        """View a ProductPeriod"""
        period = self.kwargs.get('p')
        comperiod = get_object_or_404(CompanyPeriod,
                                      company=self.kwargs.get('comp'),
                                      period = period)
        return get_object_or_404(ProductPeriod, comperiod=comperiod,
                                 product=self.kwargs.get('product'))

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the company as context that we need for breadcrumbs
        context['company'] = self.object.comperiod.company
        context['globalenv'] = self.object.comperiod.company.globalenv
        return context

class ProductPeriodNowView(ProductPeriodView):
    """Show CompanyPeriod in this current period"""
    def get_object(self, queryset=None):
        """View the current ProductPeriod"""
        comp = get_object_or_404(Company, id=self.kwargs.get('comp'))
        self.kwargs['p'] = comp.globalenv.current_period
        return super().get_object(queryset)
