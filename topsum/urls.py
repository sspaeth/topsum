"""topsum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from topsum.views import (GlobalMarketEnvListView, GlobalMarketEnvView,
                          GlobalMarketEnvCreateView, GlobalMarketEnvUpdateView,
                          GlobalMarketEnv_advance_view,
                          CompanyListView, CompanyView, CompanyCreateView,
                          CompanyUpdateView,
                          CompanyPeriodView, CompanyPeriodNowView,
                          MarketView, MarketCreateView, MarketUpdateView,
                          ProductView, ProductCreateView, ProductUpdateView,
                          ProductPeriodView, ProductPeriodNowView,
)
from django.views.generic import TemplateView

urlpatterns = [
    #path('', TemplateView.as_view(template_name="root.html")),
    path('', GlobalMarketEnvListView.as_view(), name="root"),
    path('game', GlobalMarketEnvListView.as_view(), name="game-list"),
    path('game/<int:pk>', GlobalMarketEnvView.as_view(), name="game-detail"),
    path('game/<int:pk>/advance', GlobalMarketEnv_advance_view,
         name="game-advance"),
    path('game/<int:pk>/edit', GlobalMarketEnvUpdateView.as_view(),
         name="game-edit"),
    path('game/<slug>', GlobalMarketEnvView.as_view(), name="game-detail"),
    path('game/0/new', GlobalMarketEnvCreateView.as_view(), name="game-create"),
    path('companies/<int:game_id>', CompanyListView.as_view()),
    #path('globalenv/', MarketPeriod
    #path('globalenv/', Employee
    #path('globalenv/', ProductPeriod
    #path('globalenv/<int:pk>', Product, name="product-detail"),
    #path('globalenv/', CompanyPeriod
    path('company/<int:pk>', CompanyView.as_view(), name="company-detail"),
    path('company/<slug:slug>', CompanyView.as_view(), name="company-detail"),
    path('company/<int:pk>/edit', CompanyUpdateView.as_view(), name="company-edit"),
    path('company/<slug:slug>/edit', CompanyUpdateView.as_view(), name="company-edit"),
    path('company/<int:game_id>/new', CompanyCreateView.as_view(), name="company-create"),
    path('company/<int:comp>/<int:p>', CompanyPeriodView.as_view(), name="compperiod-detail"),
    path('company/<int:comp>/now', CompanyPeriodNowView.as_view(), name="compperiod-detail-now"),
    path('market/<int:pk>', MarketView.as_view(), name="market-detail"),
    path('market/<int:game_id>/new', MarketCreateView.as_view(),
         name="market-create"),
    path('market/<int:pk>/edit', MarketUpdateView.as_view(),
         name="market-edit"),
    path('product/<int:pk>', ProductView.as_view(), name="product-detail"),
    path('product/<int:game_id>/new', ProductCreateView.as_view(),
         name="product-create"),
    path('market/<int:pk>/edit', ProductUpdateView.as_view(),
         name="product-edit"),
    path('company/<int:comp>/<int:p>/product:<int:product>', ProductPeriodView.as_view(), name="prodperiod-detail"),
    path('company/<int:comp>/now/product:<int:product>', ProductPeriodNowView.as_view(), name="prodperiod-detail-now"),
    
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
]
