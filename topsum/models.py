from enum import Enum, IntEnum

from django.contrib.auth.models import User, Group
from django.dispatch import receiver
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.urls import reverse

class GlobalMarketEnv(models.Model):
    """Represents a game and the global characteristics in it"""
    name = models.CharField(max_length=100,
                        help_text="The name of this game (max. 100 characters)")
    slug = models.SlugField(unique=True,
           help_text="Short name, used for URLs, must be unique across all "
                     "games.")
    current_period = models.PositiveSmallIntegerField(default=0,
           help_text="The current game round starting with 1.")
    total_periods = models.PositiveSmallIntegerField(default=8,
           help_text="The number of periods the game is played.")

    class STATUS(IntEnum):
        # The period has not yet begun
        NOTOPENYET = 0
        # The period is ongoing
        OPEN = 1
        # Deadline for entering has passed
        CLOSED = 2
        # Results for the period are available
        RESULTSAVAIL = 3

    STATUSNAME=('Not open yet', #0
                'Open for entry', #1
                'Entry is closed', #2
                'Results available') #3

    status = models.PositiveSmallIntegerField(choices=zip({int(s) for s in STATUS}, STATUSNAME),
                                              default=0)
    def status_name(self):
        return self.STATUSNAME[self.status]

    #associated sets: market_set, product_set, company_set

    class Meta:
        ordering = ["name"]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse ("game-detail", kwargs={'pk': self.pk})

class Period(models.Model):
    """Characteristics that all companies share in a given period"""
    time = models.PositiveSmallIntegerField(default=0)
    globalenv = models.ForeignKey('GlobalMarketEnv', on_delete=models.CASCADE,
                   help_text="The corresponding game in which this market is.")

    class Meta:
        ordering = ["globalenv", "time"]
        unique_together = [['globalenv', 'time'],]


class Market(models.Model):
    """Market segmet, instance remain constant over periods"""
    name = models.CharField(max_length=100, default="Inland",
               help_text="Name of the market (e.g. Inland).")
    slug = models.SlugField(unique=True,
               help_text="short unique name to be used in URLs.")
    globalenv = models.ForeignKey('GlobalMarketEnv', on_delete=models.CASCADE,
                   help_text="The corresponding game in which this market is.")
    
    class Meta:
        ordering = ["name"]
        unique_together = [['globalenv', 'name'],['globalenv', 'slug']]

    def __str__(self):
        return "{} (game #{})".format(self.name, self.globalenv.id)

    def get_absolute_url(self):
        return reverse ("market-detail", kwargs={'pk': self.pk})

    def get_cur_period(self):
        """Return MarketPeriod for the current period"""
        return self.globalenv.current_period


class MarketPeriod(models.Model):
    """Market characteristics in a specific period"""
    market = models.ForeignKey(Market, on_delete=models.CASCADE)
    period = models.ForeignKey('Period', on_delete=models.CASCADE)
    market_potential = models.PositiveIntegerField()
    base_interestrate = models.DecimalField(max_digits=4, decimal_places=1)


class ProductPeriod(models.Model):
    """A product/Service characteristics for a company in a given period"""
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    period = models.ForeignKey('Period', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=8, decimal_places=2,
                null=True, blank=True, default=None,
                help_text="The price in this period.")
    tech_index = models.FloatField(default=50.0,
                help_text="An index indicating the technological progress of "
                          "this product.")

    def get_absolute_url(self):
        return reverse ("prodperiod-detail",
                        kwargs={'comp': self.company.id,
                                'p': self.period, 'product': self.product.id})

    def __str__(self):
        return "{} (c={}, p={}, game=#{})".format(self.product.name,
                                            self.company, self.period,
                                            self.product.globalenv.id)


class Product(models.Model):
    """A product/Service that a company offers remaining constant over periods"""
    globalenv = models.ForeignKey('GlobalMarketEnv', on_delete=models.CASCADE)
    intro_period = models.PositiveSmallIntegerField(default= 1,
                       help_text="The period when the introduction on the "
                                 "market is possible.")
    name = models.CharField(max_length=64)
    slug = models.SlugField()

    class Meta:
        ordering = ["name"]
        unique_together = [['globalenv', 'name'],['globalenv', 'slug']]

    def __str__(self):
        return "{} (game #{})".format(self.name, self.globalenv.id)

    def get_absolute_url(self):
        return reverse ("product-detail", kwargs={'pk': self.pk})


class CompanyPeriod(models.Model):
    """A companies characteristics in a given period"""
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    period = models.ForeignKey('Period', on_delete=models.CASCADE)
    cash = models.DecimalField(max_digits=14, decimal_places=2,
                            default=10000000,
                            help_text="Money in cash account")
    empl_rd = models.PositiveSmallIntegerField(default=20,
                            help_text="Number of employees in R&D")
    empl_sales = models.PositiveSmallIntegerField(default=100,
                            help_text="Number of employees in the sales force.")
    # related: productperiod_set

    class Meta:
        ordering = ["company","period"]
        unique_together = [['company', 'period']]

    #def get_absolute_url(self):
    #    return reverse ("product-detail", kwargs={'pk': self.pk})

    def __str__(self):
        return "{} (p={})".format(self.company, self.period)

class Company(models.Model):
    """A company; this instance remains constant over all periods"""
    #employee_set is backward list of employees that can access the company
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    logo = models.ImageField(null=True, blank=True)
    globalenv = models.ForeignKey('GlobalMarketEnv', on_delete=models.CASCADE)

    class Meta:
        ordering = ["name"]
        unique_together = [['globalenv', 'name'],['globalenv', 'slug']]

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse ("company-detail", kwargs={'pk': self.pk})


class Employee(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    company = models.ForeignKey('Company', on_delete=models.CASCADE)
    role = models.CharField(max_length=100, blank=True,
               help_text="Job description of the employee")

    def __str__(self):
        return self.user.get_full_name() or self.user.get_username()


@receiver(post_save, sender=GlobalMarketEnv, dispatch_uid="createfirstMarket")
def createFirstMarket(sender, instance, created, **kwargs):
    if not created:
        return
    # Create initial default market
    market = Market(globalenv=instance, slug="inland")
    market.save()
    # Create initial default periods
    for t in range(instance.current_period+1):
        period, created = Period.objects.get_or_create(
            globalenv=instance, time=t)
        if created: period.save()


@receiver(post_save, sender=Market, dispatch_uid="createMarketPeriod")
def createMarketPeriod(sender, instance, created, **kwargs):
    if not created:
        return
    # Create MarketPeriod instance for new Market
    period = Period.objects.get(globalenv = instance.globalenv,
                                time = instance.globalenv.current_period)
    marketperiod, created = MarketPeriod.objects.get_or_create(
        market=instance, period=period)
    if created: marketperiod.save()

@receiver(post_save, sender=Company, dispatch_uid="createCompanyPeriod")
def createFirstCompanyPeriod(sender, instance, created, **kwargs):
    if not created:
        return
    # Create CompanyPeriod instance for new company
    period = Period.objects.get(globalenv = instance.globalenv,
                                time = instance.globalenv.current_period)
    comperiod, created = CompanyPeriod.objects.get_or_create(
        company=instance, period=period)
    if created: comperiod.save()
    
@receiver(post_save, sender=Product, dispatch_uid="createProductPeriod")
def createProductPeriod(sender, instance, created, **kwargs):
    if not created:
        return
    cur_period = instance.globalenv.current_period
    period = Period.objects.get(globalenv = instance.globalenv,
                                time = instance.globalenv.current_period)


    comperiods = CompanyPeriod.objects.filter(
        period = cur_period,
        company__in = instance.globalenv.company_set.all())
    for comperiod in comperiods:
        prodperiod, created = ProductPeriod.objects.get_or_create(
            product=instance, comperiod=comperiod)
        if created: prodperiod.save()


@receiver(post_save, sender=User, dispatch_uid="createTutorGroup")
def createTutorGroup(sender, instance, created, **kwargs):
    if created:
        # Create tutor group if it does not exist yet...
        tutor_group, created = Group.objects.get_or_create(name='Tutor')
        if instance.is_staff:
            instance.groups.add(tutor_group)


@receiver(post_delete, sender=Employee, dispatch_uid="deleteUser")
def employee_delete_handler(sender, instance, **kwargs):
    """Delete User if we delete an Employee without company affiliation"""
    u = instance.user
    if u.employee_set.count() == 0:
        # This user is not part of any other company
        u.delete()
